<?php
use PHPUnit\Framework\TestCase;

use RushSimpleXml\RushXML;

class RushXMLFromArrayTest extends TestCase
{
    public function testSimpleItemWithAttributes()
    {
        $xmlObject = RushXML::fromArray([
            '@rootProp1' => 'rootValue1',
            '@rootProp2' => 'rootValue2'
        ], 'Root');

        $xmlString = $xmlObject->asXml();

        $expected = new SimpleXMLElement('<Root rootProp1="rootValue1" rootProp2="rootValue2"/>');

        $this->assertEquals($xmlString, $expected->asXml(), 'The two XML strings are not equal');
    }

    public function testSimpleItemWithNested()
    {
        $xmlObject = RushXML::fromArray([
            'NestedA' => [
                'NestedB' => [
                    'NestedC' => 'val'
                ]
            ]
        ], 'Root');

        $xmlString = $xmlObject->asXml();

        $expected = new SimpleXMLElement('<Root><NestedA><NestedB><NestedC>val</NestedC></NestedB></NestedA></Root>');

        $this->assertEquals($xmlString, $expected->asXml(), 'The two XML strings are not equal');
    }

    public function testSimpleItemWithItems()
    {
        $xmlObject = RushXML::fromArray([
            'Items' => [
                ['Item' => 'a'],
                ['Item' => 'b']
            ],
        ], 'Root');

        $xmlString = $xmlObject->asXml();

        $expected = new SimpleXMLElement('<Root><Items><Item>a</Item><Item>b</Item></Items></Root>');

        $this->assertEquals($xmlString, $expected->asXml(), 'The two XML strings are not equal');
    }

    /*
     * @depends testSimpleItemWithAttributes
     * @depends testSimpleItemWithNested
     * @depends testSimpleItemWithItems
     */

    public function testRushXMLComplex()
    {
        $xmlObject = RushXML::fromArray([
            'Items' => [
                ['Item' => [
                    '@attrabute' => 'attravtive',
                    'Boom' => 'Babe'
                ]],
                ['Item' => 'B']
            ],
            'innerElement' => 'Ciao',
            'innerElement2' => 'Ciao2',
            'innerElement3' => [
                'nestedElement' => 'Ciao3',
                '@attributeA' => 'value21'
            ],
            'innerElement4' => [
                '@attributeB' => 'value22',
                '@attributeC' => 'value33',
                'sonA' => [
                    'sonb' => 'balueefe'
                ]
            ]
        ], 'Body');

        $xmlString = $xmlObject->asXml();
        $expected = new SimpleXMLElement('<Body><Items><Item attrabute="attravtive"><Boom>Babe</Boom></Item><Item>B</Item></Items><innerElement>Ciao</innerElement><innerElement2>Ciao2</innerElement2><innerElement3 attributeA="value21"><nestedElement>Ciao3</nestedElement></innerElement3><innerElement4 attributeB="value22" attributeC="value33"><sonA><sonb>balueefe</sonb></sonA></innerElement4></Body>');
        $this->assertEquals($xmlString, $expected->asXml(), 'The two XML strings are not equal');
    }

    public function testRushXMLEmptyNode()
    {
        $xmlObject = RushXML::fromArray([
            'EmptyNode' => []
        ], 'Body');

        $xmlString = $xmlObject->asXml();

        $expected =  new SimpleXMLElement('<Body><EmptyNode/></Body>');

        $this->assertEquals($xmlString, $expected->asXml(), 'The two XML strings are not equal');


        $xmlObject = RushXML::fromArray([
            'Parent' => [
                'EmptyChild' => [],
                'FilledChild' => [
                    'a' => 'b'
                ]
            ]
        ], 'Body');

        $xmlString = $xmlObject->asXml();

        $expected =  new SimpleXMLElement('<Body><Parent><EmptyChild/><FilledChild><a>b</a></FilledChild></Parent></Body>');

        $this->assertEquals($xmlString, $expected->asXml(), 'The two XML strings are not equal');

    }
    
}
?>