<?php
use PHPUnit\Framework\TestCase;


use RushSimpleXml\RushXML;

class RushXMLIterationTest extends TestCase
{
    public function testGetAttribute()
    {
        $xmlObject = RushXML::fromArray([
            '@rootProp1' => 'rootValue1',
            '@rootProp2' => 'rootValue2'
        ], 'Root');

        $attrValue = $xmlObject->getAttribute('rootProp1');
        $this->assertEquals($attrValue, 'rootValue1');

        $attrValue2 = $xmlObject->getAttribute('rootProp2');
        $this->assertEquals($attrValue2, 'rootValue2');

        $attrValue2 = $xmlObject->getAttribute('rootProp3');
        $this->assertEquals($attrValue2, false);
    }

    /*
     * @depends testIteration
     */

    public function testGetValue()
    {
        $xmlObject = RushXML::fromArray([
            'Items' => [
                ['Item' => 'a'],
                ['Item' => 'b']
            ],
        ], 'Root');

        $values = [];

        $xmlObject->Items->forEachChild(function ($child) use (&$values) {
            $values[] = $child->getValue();
        });

        $this->assertEquals($values, ['a', 'b']);
    }

    public function testIteration()
    {
        $xmlObject = RushXML::fromArray([
            'Items' => [
                '@attribute' => 'e',
                ['Item' => 'a'],
                ['Item' => 'b'],
                ['Item' => 'c']
            ],
        ], 'Root');

        $children = [];

        $xmlObject->Items->forEachChild(function($child)use(&$children){


            $children[] = (string)$child; // __toString returns string content
        });

        $this->assertEquals($children, ['a', 'b', 'c']);


        $xmlObject = RushXML::fromArray([
            'Items' => [
                ['Item' => 'a'],
                ['Item' => [
                    '@attr' => 'b',
                    'Nested' => 'd',
                ]],
                ['Item' => 'c']
            ],
            ['Other' => 'e']
        ], 'Root');

        $count = 0;

        $xmlObject->Items->forEachChild(function($child)use(&$count){

            $count++;
            $child->forEachChild(function($child)use(&$count){
                $count++;
            });
        });

        $this->assertEquals($count, 4);

    }

}
?>