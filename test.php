<?php
namespace Test;

require_once('RushSimpleXMLElement.php');

use RushSimpleXml;

$xml = new RushSimpleXml\RushSimpleXmlElement('<root>
<innerElement1>Ciao</innerElement1>
</root>');

$xml2 = RushSimpleXml\RushSimpleXmlElement::fromArray([
        'Items' => [
            ['Item' => [
                '@attrabute' => 'attravtive',
                'Boom' => 'Babe'
            ]],
            ['Item' => 'B']
        ],
        'innerElement' => 'Ciao',
        'innerElement2' => 'Ciao2',
        'innerElement3' => [
            'nestedElement' => 'Ciao3',
            '@attributeA' => 'value21'
        ],
        'innerElement4' => [
            '@attributeB' => 'value22',
            '@attributeC' => 'value33',
            'sonA' => [
                'sonb' => 'balueefe'
            ]
        ]
    ], 'Body');

echo $xml->innerElement1 . PHP_EOL;
echo $xml2->root->innerElement . PHP_EOL;
echo $xml->asXml() .PHP_EOL;
echo $xml2->asXml().PHP_EOL;