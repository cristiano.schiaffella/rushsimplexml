<?php

/**
 * RushXML
 * @author Cristiano Schiaffella
 */

// TODO add Performance test with log
// TODO add ini configuration in phpunit.xml to test with different setting

namespace RushSimpleXML;

class RushXML extends \SimpleXMLIterator {

    /**
     * Create RushXML Object from Array
     * @param array $array
     * @param string $rootElement
     * @return mixed
     */

    public static function fromArray(array $array, string $rootElement) {
        $self = new self('<'.$rootElement.'></'.$rootElement.'>');
        return self::fromArrayRecursive($self, $array);
    }

    /**
     * Recursive launched by ::fromArray()
     * @param $currentElement
     * @param $arrayContent
     * @param bool $key
     * @return mixed
     */

    private static function fromArrayRecursive(&$currentElement, $arrayContent, $key = false) {
        if ($key === false) {
            // init
            foreach($arrayContent as $currentKey => $currentContent) {
                self::fromArrayRecursive($currentElement, $currentContent, $currentKey);
            }
        } else {
            if (is_array($arrayContent)) {
                if (is_numeric($key)) {
                    // Multiple Item
                    $myKey = array_keys($arrayContent)[0];
                    self::fromArrayRecursive($currentElement, $arrayContent[$myKey], $myKey);
                } else {
                    // Node with nodes inside
                    $child = $currentElement->addChild($key);
                    foreach($arrayContent as $currentKey => $currentContent) {
                        self::fromArrayRecursive($child, $currentContent, $currentKey);
                    } 
                }
            } else {
                if (strpos($key, '@') === false) {
                    // Simple node with content
                    $currentElement->addChild($key, $arrayContent);
                } else {
                    // Attribute
                    $currentElement->addAttribute(str_replace('@', '', $key), $arrayContent);
                }
                
            }
        }

        return $currentElement;
    }

    /**
     * Retrieve RushXML node attribute value
     * @param string $attributeName
     * @return bool|string
     */

    public function getAttribute(string $attributeName)
    {
        if ($this[$attributeName]) {
            return (string)$this[$attributeName];
        } else {
            return false;
        }
    }

    /**
     * Returns the string value contained in node
     * @return string
     */

    public function getValue()
    {
        return (string) $this;
    }

    /**
     * Iterate over each child with a callable
     * @param callable $iterator
     * @return mixed
     */

    public function forEachChild(callable $iterator)
    {
        foreach($this->children() as $name => $data) {
            $iterator($data);
        }
    }
}
